# GIS Tool Geese foraging areas

## Description 

This tool creates GIS vector maps of geese foraging areas. The tool starts with a selection of agricultural fields from the Dutch Basisregistratie Percelen ([BRP](https://data.overheid.nl/dataset/10674-basisregistratie-gewaspercelen--brp-)). Disturbing elements and a buffer zone around them are then cut away from the fields. What remains is the foragingmap.

Currently the following disruptive elements are offered in the tool:
 * buildings (from [BAG](https://bagviewer.kadaster.nl/lvbag/bag-viewer/index.html))
 * roads (from [Nationaal Wegenbestand](https://www.nationaalwegenbestand.nl/wat-het-nwb))
 * terrain features, derived from the [AHN3](https://www.ahn.nl/) digital terrain- and surface models.

The outer boundary of the foraging area is formed by the provincial border(s). 

Attribute information from a provincial level geese-foraging map (such as the [Ganzenfourageergebiedkaart](https://geoPortal.fryslan.nl/arcgis/rest/services/Themas/natuur/MapServer/14) of the Province of Fryslan) can be linked at field level to the remaining BRP fields. This includes the relevant Agricultural Nature Management association. Currently, additional information for Friesland, Noord-Holland, Utrecht and Zeeland is supported.   

## Demo
![screenshot](resources/demo_ganzen_fourage.png)

Permanent grassland (BRP-code 265) as Geese foraging area, reduced with:
1. 100m buffer around BAG buildings
2. 100m buffer around roads
3. 100m buffer around terrain elements (DSM-DTM difference >= 3m)


![demo_attributen](resources/screenshot2.JPG)
Where known, information from the Provincial Goose Forage Map has been added. The selected plot is part of ANV Om'e Kuizen.

## Installation
The tool is developed in Python and requires several modules in addition to the [Python Standard Library](https://docs.python.org/3/library/index.html). These are:

 * [numpy](https://numpy.org/) 1.25.2
 * [pandas](http://pandas.pydata.org/) 2.0.3
 * [geopandas](https://geopandas.org/en/stable/) 0.14.0
 * [rasterio](https://rasterio.readthedocs.io/en/latest/index.html) 1.3.6
 * [scikit-image](https://scikit-image.org/) 0.20.0

(version numbers are the latest versions tested, newer versions may also work).

To install the tool: 
  * download or [clone](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository) the repository to a local directory, eg. c:\tools\ganzenkaart
  * download [Miniconda](https://docs.conda.io/projects/miniconda/en/latest/)
  * Create a Conda Environment containing the earlier mentioned modules
  * navivate in CMD to the repository and enter: `C:\tools\ganzenkaart\src> python ganzen_kaart.py --help`

The tool uses GeoData from the WUR network: `W:\projects\GeoData` and `W:\projects\GeoDeskData`. The program checks whether these locations are accessible. All required GeoData is mentioned [here](https://git.wur.nl/roelo008/ganzen/-/blob/main/src/brondata.py?ref_type=heads#L7).

## Use
Before using the tool, please update the **local_data_directory** variable in `src\constants.py`. This directory should contain the Shapefiles for Utrecht, Noord-Holland, Zeeland and Scenario01 foraging-areas.

The tool has *one* mandatory parameter (the output directory) and 12 optional parameters.  

```commandline
positional arguments:
  output_dir            output directory

optional arguments:
  --prov                een of meer provincies. Default: “Friesland”. Choices are: [Drenthe, Flevoland, Friesland, Gelderland, Groningen, Limburg, Noord-Brabant, Noord-Holland, Overijssel, Utrecht, Zeeland, Zuid-Holland.]
  --brp                 een of meer BRP gewascodes. Default: 265.
  --terrain             minimale DSM-DTM verschil in m. Default: 3.
  --road_buffer         eenzijdige buffer breedte in m. rondom wegen. Default: 100.
  --bag_buffer          eenzijdige buffer breedte in m. rondom gebouwen. Default: 100.
  --terrain_buffer      eenzijdige buffer breedte in m. rondom terrein elementen. Default: 100.
  --no_roads            do not subtract roads. Default: False.
  --no_bag              do not subtract buildings. Default: False.
  --no_terrain          do not subtract terrain-items. Default: False.
  --leefgebied          join attributes from provincial leefgebied. Choices are: [Friesland, ganzenrustgebied_noordholland, ganzenrustgebied_utrecht, ganzenrustgebied_zeeland, leefgebied_scenario01, dummy_scenario]
  --predicate           spatial join predicate between resources and leefgebied. Choices are: [intersects (default), within]
  --sample              test run with 25 BRP fields. Default: False.

options:
  -h, --help  show this help message and exit
```

## Scenarios
Users can provide scenario geospatial data. Attributes of the scenario-areas will be joined to the fourageergebieden. Provide the data directory, shapefile name and use-attributes in the `SCENARIOS` dictionary in `src/constants.py`. To employ a scenario, use the `leefgebied` parameter.

## Examples
Map for the Province of Drenthe, without roads and terrain buffers, 50m buffer around buildings, BRP codes 265 and 266 as a starting point:

`python ganzen_kaart.py c:/ganzen/kaart01 --prov Drenthe --brp 265 266 --bag_buffer 50 --no_roads --no_terrain`

Map for Groningen and Friesland, without buildings, at least 6m DSM-DTM difference:

`python ganzen_kaart.py c:/ganzen/kaart01 --prov Friesland Groningen --terrain 6 --no_bag`

Map with all default options:

`python ganzen_kaart.py c:/apps/kaart01`

Allowed options for `--prov` are: 
1. Drenthe
2. Flevoland
3. Friesland
4. Gelderland
5. Groningen
6. Limburg
7. Noord-Brabant
8. Noord-Holland
9. Overijssel
10. Utrecht
11. Zeeland
12. Zuid-Holland

`--brp` accepts *one* or more valid BRP crop codes. The permitted codes are listed [here](https://git.wur.nl/roelo008/ganzen/-/blob/main/src/brondata.py?ref_type=heads#L212). For more information about crop codes, see the [RVO crop code list](https://data.rvo.nl/gewascodelijst).
 
## Support
Get in touch with [Hans Roelofsen](https://www.wur.nl/en/Persons/Hans-dr.-HD-Hans-Roelofsen.htm)

## Development 
* parallel processing DSM - DTM

## Authors and acknowledgment
Tool development: [Hans Roelofsen](https://www.wur.nl/nl/personen/hans-roelofsen.htm)  
Tool commissioner: [Hans Baveco](https://www.wur.nl/en/Persons/Hans-dr.-JM-Hans-Baveco.htm)

## License
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)

