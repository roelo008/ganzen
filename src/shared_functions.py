import os
import geopandas as gp

try:
    from src.constants import SOURCEDATA, LOCAL_DATA_DIR, SCENARIOS
except ModuleNotFoundError:
    from constants import SOURCEDATA, LOCAL_DATA_DIR, SCENARIOS


def verify_sourcedata():
    """
    Verify brondata exists and is accessible
    :return:
    """

    if not os.path.isdir(LOCAL_DATA_DIR):
        raise NotADirectoryError(f"{LOCAL_DATA_DIR} does not exist or is inaccesible.")

    for k, v in SOURCEDATA.items():
        if v["dir"].startswith("https"):
            pass
        elif not os.path.exists(v["dir"]):
            raise NotADirectoryError(f'{v["dir"]} does not exist or is inaccesible.')
        elif v["dir"].endswith(("gdb", "gpkg")):
            try:
                _ = gp.read_file(v["dir"], layer=v["lyr"], rows=1)
            except ValueError:
                raise FileExistsError(
                    f'layer {v["lyr"]} does not exist within {v["dir"]}.'
                )
        else:
            if not os.path.isfile(os.path.join(v["dir"], v["lyr"])):
                raise FileExistsError(
                    f'layer {v["lyr"]} does not exist within {v["dir"]}.'
                )


def read_gis(dir: str, lyr: str, sample: bool = False, **kwargs) -> gp.GeoDataFrame:
    """
    Read either shapefile from disk or layer from Geopacakge
    :param sample: use 100 rows only
    :param dir: file directory or GeoPackage
    :param lyr: shapefile name within file dir or GPKG layer name
    :return: geopandas geodataframe
    """
    try:
        if dir.endswith(("gpkg", "gdb")):
            params = {"filename": dir, "layer": lyr}
        else:
            params = {"filename": os.path.join(dir, lyr)}

        gdf = gp.read_file(**params, **kwargs)

        if sample:
            return gdf.sample(100)
        else:
            return gdf

    except ValueError as e:
        print("Incorrect layer source or name: ".format(e))
        raise


def gdf_buffer(gdf: gp.GeoDataFrame, dist: int) -> gp.GeoDataFrame:
    """
    Buffer a polygon geodataframe, to create a single-part polygon gdb with spatial overlap removed
    :param gdf:
    :param dist:
    :return:
    """

    gdf["geometry"] = gdf.buffer(distance=dist)
    return gdf


def difference(src: gp.GeoDataFrame, overlay: gp.GeoDataFrame) -> gp.GeoDataFrame:
    """
    Apply spatial difference operation. Return GeoDataFrame as input_lyr, with
    areas overlapping with overlay removed.

    Difference works best 1:1 with a single multi-polygon feature in both input and overlay.


    :param src:
    :param overlay:
    :return:
    """

    out = gp.overlay(src, overlay, how="difference", keep_geom_type=True)
    if out.empty or out.is_empty.any():
        raise ValueError("Empty geometry created.")
    else:
        return out
