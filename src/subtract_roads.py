import geopandas as gp

try:
    from src.constants import SOURCEDATA
    from src.shared_functions import read_gis, gdf_buffer
except ModuleNotFoundError:
    from constants import SOURCEDATA
    from shared_functions import read_gis, gdf_buffer


ROADS_INCLUDE_FIELDS = ["WVK_ID", "WEGTYPE", "geometry"]


def get_roads(
    bbox: gp.GeoDataFrame,
    buffer: int = 100,
) -> gp.GeoDataFrame:
    print("Fetching roads")
    return gdf_buffer(
        read_gis(
            SOURCEDATA["wegen"]["dir"],
            SOURCEDATA["wegen"]["lyr"],
            bbox=bbox,
            include_fields=ROADS_INCLUDE_FIELDS,
        ).set_crs("EPSG:28992", allow_override=True),
        dist=buffer,
    )
