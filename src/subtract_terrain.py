import affine
import os.path
import rasterio as rio
import numpy as np
import geopandas as gp
from typing import Tuple
from skimage import morphology
from rasterio.windows import Window
from rasterio.coords import BoundingBox
from rasterio.features import shapes

try:
    from src.constants import SOURCEDATA
    from src.shared_functions import read_gis
except ModuleNotFoundError:
    from constants import SOURCEDATA
    from shared_functions import read_gis


def snap_bounds_outwards(gdf: gp.GeoDataFrame, interval: int = 1000) -> BoundingBox:
    """
    Snap bounds from a GeoDataFrame to nearest 1000m
    Snap upwards for right and top
    Snap downwards for left and bottom
    :param interval:
    :return:
    """

    bounds = gdf.bounds.iloc[0].to_dict()
    return BoundingBox(
        left=(bounds["minx"] // interval) * interval,
        bottom=(bounds["miny"] // interval) * interval,
        right=((bounds["maxx"] // interval) * interval) + interval,
        top=((bounds["maxy"] // interval) * interval) + interval,
    )


def bounds_to_window(
    bounds: BoundingBox, transform: affine.Affine
) -> Tuple[Window, affine.Affine]:
    """
    Transform bounds in projected coordinates to a rio Window
    :param bounds:
    :param transform:
    :return:
    """

    w = rio.windows.from_bounds(
        left=bounds.left,
        bottom=bounds.bottom,
        right=bounds.right,
        top=bounds.top,
        transform=transform,
    )
    t = affine.Affine(
        a=transform.a,
        b=transform.b,
        c=bounds.left,
        d=transform.d,
        e=transform.e,
        f=bounds.top,
    )

    return w, t


def identify_raised_surfaces(
    dtm: np.array,
    dsm: np.array,
    minimum_difference: int = 3,
    true_value: int = 1,
    false_vale: int = 0,
) -> np.array:
    """
    Compare DSM and DTM and return 1/0 array indicating where difference is >= minimum differences
    :param dtm:
    :param dsm:
    :param minimum_difference:
    :return:
    """

    return np.where(
        np.subtract(dsm, dtm) >= minimum_difference, true_value, false_vale
    ).astype(np.uint8)


def buffer_binary_array(arr: np.array, buffer_pxls: int) -> np.array:
    """

    Buffer binary array

    :param arr:
    :param buffer_pxls:
    :return:
    """

    return morphology.binary_dilation(arr, morphology.disk(buffer_pxls)).astype(
        np.uint8
    )


def verify_matching_rasters(
    raster_a: rio.DatasetReader, raster_b: rio.DatasetReader
) -> bool | AssertionError:
    """

    Verify that two geospatial rasters have identical bounds and origin

    :param raster_a:
    :param raster_b:
    :return:
    """

    origin = (raster_a.transform.c, raster_a.transform.f) == (
        raster_b.transform.c,
        raster_b.transform.f,
    )
    resolution = raster_a.transform.a == raster_b.transform.a
    if origin and resolution:
        return True
    else:
        raise ValueError(f"Rasters do not match")


def array_to_gdf(
    arr: np.array,
    transform: affine.Affine,
    crs: rio.crs.CRS,
    true_value: int = 1,
) -> gp.GeoDataFrame:
    """

    Vectorize raster array to geodataframe

    :param transform:
    :param true_value:
    :param arr:
    :param crs:
    :return:
    """

    results = (
        {"properties": {"raster_val": v}, "geometry": s}
        for i, (s, v) in enumerate(shapes(arr, transform=transform))
        if v == true_value
    )
    return gp.GeoDataFrame.from_features(list(results)).set_crs(crs)


def get_terrain(
    bbox: gp.GeoDataFrame,
    minimum_difference: int = 3,
    buffer_distance: int = 100,
) -> gp.GeoDataFrame:
    """

    :param minimum_height:
    :param buffer_distance:
    :param bbox:
    :return:
    """

    print("Fetching AHN3 DTM & DSM")

    snap_bbox = snap_bounds_outwards(bbox)
    dsm = rio.open(
        os.path.join(SOURCEDATA["ahn3_dsm"]["dir"], SOURCEDATA["ahn3_dsm"]["lyr"])
    )
    dtm = rio.open(
        os.path.join(SOURCEDATA["ahn3_dtm"]["dir"], SOURCEDATA["ahn3_dtm"]["lyr"])
    )
    _ = verify_matching_rasters(dsm, dtm)
    read_window, read_transform = bounds_to_window(snap_bbox, transform=dtm.transform)

    opgaand_array = identify_raised_surfaces(
        dtm=dtm.read(1, window=read_window),
        dsm=dsm.read(1, window=read_window),
        minimum_difference=minimum_difference,
    )

    buffered_array = buffer_binary_array(
        opgaand_array, int(buffer_distance / dsm.transform.a)
    )

    return array_to_gdf(buffered_array, crs=dsm.crs, transform=read_transform)
