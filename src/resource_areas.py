import geopandas as gp
from typing import List

try:
    from src.constants import SOURCEDATA
    from src.shared_functions import read_gis
except ModuleNotFoundError:
    from constants import SOURCEDATA
    from shared_functions import read_gis

BRP_USE_COLUMNS = ["id", "GWS_GEWASCODE"]
PROVINCIE_IDENTIFIER_COLUMN = "provincienaam"


def get_aoi(provincies: List[str]) -> gp.GeoDataFrame:
    """

    :param provincies:
    :return:
    """

    print("Fetching area of interest")

    query = ", ".join([f"'{i}'" for i in provincies])
    gdf = read_gis(
        SOURCEDATA["provincies"]["dir"],
        SOURCEDATA["provincies"]["lyr"],
    )
    gdf = gdf.query(f"{PROVINCIE_IDENTIFIER_COLUMN} in [{query}]")
    if not gdf.empty:
        return gdf.dissolve()
    else:
        raise ValueError(f"No AOI found for provincie(s) {provincies}")


def get_brp(
    bbox: gp.GeoDataFrame,
    gewascodes: List[int],
) -> gp.GeoDataFrame:
    """

    :param bbox:
    :param gewascodes:
    :return:
    """

    print("Fetching BRP percelen")

    query = ", ".join([f"'{i}'" for i in gewascodes])
    query2 = f"{BRP_USE_COLUMNS[1]} in [{query}]"
    # Note: GWS_GEWASCODE is TEXT field, not Integer!
    resources = read_gis(
        SOURCEDATA["brp"]["dir"],
        SOURCEDATA["brp"]["lyr"],
        bbox=bbox,
        include_fields=BRP_USE_COLUMNS,
    )
    resources = resources.query(query2)

    if resources.empty:
        raise ValueError(
            f'no resources found in {SOURCEDATA["brp"]["lyr"]} query {query2}'
        )
    else:
        return resources
