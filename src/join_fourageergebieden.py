import os
import numpy as np
import requests
import urllib.parse
import pandas as pd
import geopandas as gp
from typing import List

from attr import dataclass

try:
    from src.constants import (
        SOURCEDATA,
        SCENARIOS,
        JOIN_ATTRIBUTE_NAMES,
        JOIN_ATTRIBUTE_DEFAULT_VALUES,
    )
    from src.shared_functions import read_gis
except ModuleNotFoundError:
    from constants import (
        SOURCEDATA,
        SCENARIOS,
        JOIN_ATTRIBUTE_NAMES,
        JOIN_ATTRIBUTE_DEFAULT_VALUES,
    )
    from shared_functions import read_gis


def download_from_esri_rest(
    url: str,
    use_fields: List[str],
) -> gp.GeoDataFrame:
    """
    Query and download from an ESRI rest Feature Service
    :param of: output format
    :param url:
    :param use_fields:
    :return:
    """

    base_url = url

    # Verify service uptime
    status = requests.get(base_url).status_code
    if status > 299:
        raise requests.HTTPError(f"{base_url} unavailable with code {status}.")

    # Get basic properties of the service
    max_records = requests.get(
        base_url + "?" + urllib.parse.urlencode({"f": "pjson"})
    ).json()["maxRecordCount"]

    # Get nr of records in the service
    n_records = requests.get(
        base_url
        + "/query?"
        + urllib.parse.urlencode(
            {
                "returnCountOnly": "true",
                "returnGeometry": "false",
                "where": "1=1",
                "f": "pjson",
            }
        )
    ).json()["count"]

    if max_records < n_records:
        # Identify IDs of all features
        ids = requests.get(
            base_url
            + "/query?"
            + urllib.parse.urlencode(
                {
                    "returnCountOnly": "false",
                    "returnGeometry": "false",
                    "where": "1=1",
                    "f": "pjson",
                    "returnIdsOnly": "true",
                }
            )
        ).json()["objectIds"]

        # Create batches with size equal to max API request
        ids_batches = np.array_split(
            np.array(ids),
            np.divmod(n_records, max_records)[0] + 1,
        )
        holder = []
        for start, stop in zip(
            [min(arr) for arr in ids_batches], [max(arr) for arr in ids_batches]
        ):
            contents = requests.get(
                base_url
                + "/query?"
                + urllib.parse.urlencode(
                    {
                        "f": "geojson",
                        "outFields": ",".join(use_fields),
                        "where": f"ObjectID >= {start} AND ObjectID < {stop}",
                    }
                )
            )
            holder.append(gp.read_file(contents.text, driver="GeoJSON"))

        return gp.GeoDataFrame(pd.concat(holder))

    else:
        contents = requests.get(
            base_url
            + "/query?"
            + urllib.parse.urlencode(
                {
                    "f": "geojson",
                    "outFields": ",".join(use_fields),
                    "where": "1=1",
                }
            )
        )
        return gp.read_file(contents.text, driver="GeoJSON")


def project_to_rdnew(gdf: gp.GeoDataFrame) -> gp.GeoDataFrame:
    """

    :param gdf:
    :param crs:
    :return:
    """
    return gdf.to_crs("EPSG:28992")


def poly_to_point(gdf: gp.GeoDataFrame) -> gp.GeoDataFrame:
    """

    :param gdf:
    :return:
    """
    gdf.geometry = gdf.geometry.centroid
    return gdf


def fourageer_friesland() -> gp.GeoDataFrame:
    """
    Get fourageergebiedenin in Friesland

    :return:
    """

    return project_to_rdnew(
        download_from_esri_rest(
            url=os.path.join(
                SCENARIOS["fourageergebied_friesland"]["dir"],
                SCENARIOS["fourageergebied_friesland"]["lyr"],
            ),
            use_fields=JOIN_ATTRIBUTE_NAMES,
        )
    )


class FourageerGebied:
    def __init__(self, scenario_naam: str):
        self.scenario_naam = scenario_naam

    def __call__(self, *args, **kwargs) -> gp.GeoDataFrame:
        """
        Read scenario data from file

        :param scenario_naam:
        :return:
        """

        return read_gis(
            dir=SCENARIOS[self.scenario_naam]["dir"],
            lyr=SCENARIOS[self.scenario_naam]["lyr"],
            include_fields=["geometry"] + SCENARIOS[self.scenario_naam]["use_fields"],
        ).assign(**dict(zip(JOIN_ATTRIBUTE_NAMES, JOIN_ATTRIBUTE_DEFAULT_VALUES)))


def get_fourageergebieden(as_points: bool = True, *targets) -> gp.GeoDataFrame:
    """
    :return:
    """

    fourageer_gebieden = {
        "Friesland": fourageer_friesland,
    }

    for scenarionaam in SCENARIOS.keys():
        fourageer_gebieden[scenarionaam] = FourageerGebied(scenarionaam)

    holder: List[gp.geodataframe] = []
    for target in targets:
        try:
            f = fourageer_gebieden[target]
            holder.append(f())
        except KeyError as e:
            print(f"{target} invalid request")

    fourageergebieden = gp.GeoDataFrame(pd.concat(holder, axis=0))

    if as_points:
        return poly_to_point(fourageergebieden)
    else:
        return fourageergebieden
