import geopandas as gp

try:
    from src.constants import SOURCEDATA
    from src.shared_functions import read_gis, gdf_buffer
except ModuleNotFoundError:
    from constants import SOURCEDATA
    from shared_functions import read_gis, gdf_buffer

BAG_INCLUDE_FIELDS = [
    "Identificatie",
    "Gebruiksfunctie",
    "Bouwjaar",
    "Gebied",
    "geometry",
]


def get_buildings(
    bbox: gp.GeoDataFrame,
    buffer: int = 100,
) -> gp.GeoDataFrame:
    print("Fetching BAG buildings")

    return gdf_buffer(
        read_gis(
            SOURCEDATA["bag"]["dir"],
            SOURCEDATA["bag"]["lyr"],
            bbox=bbox,
            include_fields=BAG_INCLUDE_FIELDS,
        ).set_crs("EPSG:28992", allow_override=True),
        dist=buffer,
    )
