import sys
import geopandas as gp
import datetime
from requests import HTTPError
from typing import List

try:
    from src.constants import (
        PROVINCIES,
        BRP_GEWASCODES,
        JOIN_ATTRIBUTE_NAMES,
        JOIN_ATTRIBUTE_DEFAULT_VALUES,
        SCENARIOS,
    )
    from src.shared_functions import difference, verify_sourcedata
    from src.resource_areas import get_aoi, get_brp
    from src.subtract_buildings import get_buildings
    from src.subtract_terrain import get_terrain
    from src.subtract_roads import get_roads
    from src.join_fourageergebieden import get_fourageergebieden
except ModuleNotFoundError:
    from constants import (
        PROVINCIES,
        BRP_GEWASCODES,
        JOIN_ATTRIBUTE_NAMES,
        JOIN_ATTRIBUTE_DEFAULT_VALUES,
        SCENARIOS,
    )
    from shared_functions import difference, verify_sourcedata
    from resource_areas import get_aoi, get_brp
    from subtract_buildings import get_buildings
    from subtract_terrain import get_terrain
    from subtract_roads import get_roads
    from join_fourageergebieden import get_fourageergebieden


def to_list(foo) -> list:
    """
    if foo is a list, return as is
    else return [foo]
    :param foo:
    :return:
    """

    if isinstance(foo, list):
        return foo
    else:
        return [foo]


def build_ganzen_fourageer_kaart(**kwargs):
    """

    :param kwargs:
    :return:
    """

    # Read area of interest
    aoi = get_aoi(provincies=to_list(kwargs.get("prov")))

    # Get initial resource area from BRP
    resources = get_brp(
        bbox=aoi,
        gewascodes=to_list(kwargs.get("brp")),
    )

    # Clip resources to AOI
    resources = gp.clip(resources, aoi)

    if kwargs.get("sample"):
        resources = resources.sample(100)

    # Subtract areas where (DSM - DTM) >= XXm
    if not kwargs.get("no_terrain"):
        resources = difference(
            resources,
            get_terrain(
                bbox=resources,
                minimum_difference=kwargs.get("terrain"),
                buffer_distance=kwargs.get("terrain_buffer"),
            ),
        )

    # Subtract buffered roads
    if not kwargs.get("no_roads"):
        resources = difference(
            resources,
            get_roads(bbox=resources, buffer=kwargs.get("road_buffer")),
        )

    # Subtract buffered buildings
    if not kwargs.get("no_bag"):
        resources = difference(
            resources,
            get_buildings(bbox=resources, buffer=kwargs.get("bag_buffer")),
        )

    # Attach information on proviciale fourageergebieden
    if kwargs.get("leefgebied"):
        fourageergebieden = get_fourageergebieden(False, *args.leefgebied)
        resources = (
            gp.sjoin(
                left_df=resources,
                right_df=fourageergebieden,
                how="left",
                predicate=args.predicate,
            )
            .drop(columns=["index_right"])
            .fillna(dict(zip(JOIN_ATTRIBUTE_NAMES, ["geen", "geen", 0])))
            .astype({JOIN_ATTRIBUTE_NAMES[2]: int})
        )

    # write to file
    resources.to_file(
        os.path.join(
            kwargs.get("output_dir"),
            f'wenr_ganzen_fourageerkaart_{datetime.datetime.now().strftime("%Y%m%d_%H%M")}.shp',
        )
    )


if __name__ == "__main__":
    import os
    import argparse

    def valid_directory(path: str):
        if os.path.isdir(path):
            return path
        else:
            raise argparse.ArgumentTypeError(f"{path} is not a valid directory")

    def int_gte1(value: int) -> bool:
        ivalue = int(value)
        if isinstance(ivalue, int) and ivalue >= 1:
            return ivalue
        else:
            raise argparse.ArgumentTypeError(f"{ivalue} should be >= 1.")

    prog_description = (
        "**NOTE**\n"
        "**Update local data directory in src/constants.py before use**\n\n"
        "positional arguments:\n"
        "  output_dir            output directory\n\n"
        "optional arguments:\n"
        f"  --prov                een of meer provincies. Default: “Friesland”. Choices are: [{', '.join(PROVINCIES)}.]\n"
        "  --brp                 een of meer BRP gewascodes. Default: 265.\n"
        "  --terrain             minimale DSM-DTM verschil in m. Default: 3.\n"
        "  --road_buffer         eenzijdige buffer breedte in m. rondom wegen. Default: 100.\n"
        "  --bag_buffer          eenzijdige buffer breedte in m. rondom gebouwen. Default: 100.\n"
        "  --terrain_buffer      eenzijdige buffer breedte in m. rondom terrein elementen. Default: 100.\n"
        "  --no_roads            do not subtract roads. Default: False.\n"
        "  --no_bag              do not subtract buildings. Default: False.\n"
        "  --no_terrain          do not subtract terrain-items. Default: False.\n"
        f"  --leefgebied          join attributes from provincial leefgebied. Choices are: [Friesland, {', '.join(list(SCENARIOS.keys()))}]\n"
        "  --predicate           spatial join predicate between resources and leefgebied. Choices are: [intersects (default), within]\n"
        "  --sample              test run with 25 BRP fields. Default: False.\n"
    )

    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        argument_default=argparse.SUPPRESS,
        prog="Ganzen Fouragekaart",
        description=prog_description,
        usage="see https://git.wur.nl/roelo008/ganzen/",
    )
    parser.add_argument(
        "output_dir",
        # help="output directory",
        type=valid_directory,
        help=argparse.SUPPRESS,
    )
    parser.add_argument(
        "--prov",
        help=argparse.SUPPRESS,
        choices=PROVINCIES,
        nargs="+",
        default="Friesland",
        type=str,
    )
    parser.add_argument(
        "--brp",
        help=argparse.SUPPRESS,
        type=int,
        nargs="+",
        choices=BRP_GEWASCODES,
        default=265,
    )
    parser.add_argument(
        "--road_buffer",
        help=argparse.SUPPRESS,
        default=100,
        type=int_gte1,
    )
    parser.add_argument(
        "--bag_buffer",
        help=argparse.SUPPRESS,
        default=100,
        type=int_gte1,
    )
    parser.add_argument("--terrain", help=argparse.SUPPRESS, type=int_gte1, default=3)
    parser.add_argument(
        "--terrain_buffer",
        help=argparse.SUPPRESS,
        default=100,
        type=int_gte1,
    )
    parser.add_argument(
        "--leefgebied",
        help=argparse.SUPPRESS,
        choices=list(SCENARIOS.keys())
        + ["Friesland", "Utrecht", "Noord-Holland", "Zeeland"],
        nargs="+",
        type=str,
    )
    parser.add_argument(
        "--predicate",
        choices=["within", "intersects"],
        default="intersects",
        type=str,
        help=argparse.SUPPRESS,
    )
    parser.add_argument("--sample", help=argparse.SUPPRESS, action="store_true")
    parser.add_argument("--no_roads", help=argparse.SUPPRESS, action="store_true")
    parser.add_argument("--no_bag", help=argparse.SUPPRESS, action="store_true")
    parser.add_argument("--no_terrain", help=argparse.SUPPRESS, action="store_true")
    args = parser.parse_args()

    try:
        print("Commencing...")
        verify_sourcedata()
        build_ganzen_fourageer_kaart(**vars(args))
        print("Done!")
    except (ValueError, FileExistsError, HTTPError, NotADirectoryError) as e:
        print(f"!! Error: {e}")
        sys.exit(0)
